//Connection au serveur
var socket = io.connect(window.location.host);
var pressure;
var temperature;
var humidity;
//évènement qui récupère les données du capteur DHT
socket.on("GetDHTValue", function(data) {
    //Création de la date actuelle
    var today = new Date();
    if (data[0] != undefined && data[1] != undefined) {
        $("#currentDate").text(today.toLocaleString());
        temperature = data[0]
        $("#temperature").text(data[0] + "°C");
        humidity = data[1]
        $("#humidity").text(data[1] + "%");
    }
});

socket.on("GetBarometerValue", function(data) {
    //Création de la date actuelle
    var today = new Date();
    pressure = data
    if (data != 0) {
        $("#currentDate").text(today.toLocaleString());
        $("#pressure").text(data + " hPa")
    }
});


socket.on("GetLightValue", function(data) {
    //Création de la date actuelle
    var today = new Date();

    if(data != null){
        $("#currentDate").text(today.toLocaleString());
        $("#light").text(data + " lx")
    }
});



var quarter = 10000
window.setInterval(function() {
    var now = new Date();
    socket.emit("RequestPressureOneHourAgo", now.getTime());
}, quarter);


socket.on("ReturnPressureOneHourAgo", function(pressureOneHourAgo){

    $("#forecast").empty();
    $("#hpah").empty();

    var forecastIcon;
    var prev 

    if (pressureOneHourAgo == 0)
    {
        $("#hpah").append('<i class="wi wi-moon-alt-full forecastIcon"></i>');
        $("#forecast").append('<i class="wi wi-moon-alt-full forecastIcon"></i>');
        socket.emit("SendTwittWithoutForecast", addressOfStation, temperature, humidity, pressure);
    }

    else{

        //Variation de la pression
        var deltaPressure = Math.round((pressure - pressureOneHourAgo) * 10) / 10;
        $("#hpah").append('<div id = "hpahNumber">' + deltaPressure + '</div>');

        if(deltaPressure > 0.1)
        {
            icon = ""
            $("#hpah").append('<i class="wi wi-direction-up-right" id="upOrDown"></i>');
        }

        else if (deltaPressure < -0.1)
        {
            $("#hpah").append('<i class="wi wi-direction-down-right" id="upOrDown"></i>');
        }

        else{
             $("#hpah").append('<i class="wi wi-direction-right" id="upOrDown"></i>');
         }

        //Prévision

        if (pressure <= middlePressure)
        {

            if (deltaPressure < 0){
                forecastIcon = "wi-rain forecastIcon";
                prev = "mauvais temps"
            }

            else if ((deltaPressure*2) + pressure > middlePressure){
                forecastIcon = "wi-day-cloudy";
                prev = "beau temps"
            }

            else 
            {
                forecastIcon = "wi-cloudy";
                prev = "temps stable"
            }
        }

        else if (pressure > middlePressure)
        {
            if (deltaPressure > 0){
                forecastIcon = "wi-day-sunny"
                prev = "beau temps"
            }

            else if ((deltaPressure*2) + pressure < middlePressure){
                forecastIcon = "wi-day-rain"
                prev = "mauvais temps"
            }

            else {
                forecastIcon = "wi-day-sunny-overcast"
                prev = "temps stable"
            }

        }

    $("#forecast").append('<i class="wi '+ forecastIcon +' forecastIcon"></i>');
    socket.emit("SendTwittWithForecast", prev, addressOfStation, temperature, humidity, pressure);

}


});


var lat;
var longi;
var alt;
var middlePressure;

socket.on("GetLocation", function(loc){


   loc = loc[loc.length-1];
   loc = loc.slice(1,loc.length-1);

   var locList = loc.split(",");

   alt = locList[0];
   middlePressure = 1013.25 * (Math.pow(1-((0.0065*alt)/288.15), 5.255));
   lat = Math.round(locList[1] * 100000) / 100000;
   longi = Math.round(locList[2] * 100000) / 100000;

   moveToLocation(lat,longi);
   geocodeLatLng(geocoder, lat, longi);
    $("#map").show();
    google.maps.event.trigger(map, 'resize');
});

var geocoder
var map;
var addressOfStation;

function initMap() {

    $("#map").hide()
    map = new google.maps.Map(document.getElementById('map'), {
    center: new google.maps.LatLng(46.947048, 7.437098),
    zoom: 18,
    mapTypeId: google.maps.MapTypeId.SATELLITE

  });

    geocoder =  new google.maps.Geocoder;
}

function moveToLocation(lat, lng){
    var center = new google.maps.LatLng(lat+0.00055, lng-0.0031);
    map.panTo(center);

    var marker = new google.maps.Marker({
    position: new google.maps.LatLng(lat,lng),
    map: map,
    title: 'Localisation de la station'
  });
}


function geocodeLatLng(geocoder, lt, lg) {
console.log("géo")
  var latlng = {lat: parseFloat(lt), lng: parseFloat(lg)};
  geocoder.geocode({'location': latlng}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        addressOfStation = results[2].formatted_address
        console.log(addressOfStation)
        }
       else {
        console.log('No results found');
      }
    } else {
      console.log('Geocoder failed due to: ' + status);
    }
  });
}