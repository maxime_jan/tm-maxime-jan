//Chargement des modules pour le serveur Node
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var fs = require('fs');
var GrovePi = require("./sensors");
var port = 8080;

//Création du serveur http
app.get('/', function(req, res) {
    res.sendFile("/home/pi/tm-maxime-jan/source/client/html" + '/index.html');
});

//Définition d'un fichier statique
app.use(express.static("/home/pi/tm-maxime-jan/source/client/static"));

//Mise en écoute du serveur sur le port indiqué
server.listen(port, function() {
    console.log("Le serveur a été démarré sur le port " + port);
    GrovePi.startGP();
});

//Ecoute de socket.io dans le fichier "socketio.js"
var socketIOFile = require("./socketio")(io);