var GrovePi = require("./sensors");
var dataHandler = require("./dataHandler");
var currentDHTValue;
var currentBarometerValue;
var currentLightValue = 0;


var Twitter = require('twitter');
 
var client = new Twitter({
  consumer_key: 'BKXMw7hzhXArVfVhcUsKt5AVB',
  consumer_secret: 'sNU203dvfMmWvI9TldazZHmFBRyKF3FoBnemmU0WiGVVsCLJfp',
  access_token_key: '732117257107705856-E4BalRcr5YAGAfrgN48oxPWL5HMgZUc',
  access_token_secret: 'kZ6B6MPw8piwxlKYqHEahmPHiXVtFUc7LlFegStuJ0RMh'
});


var PythonShell = require('python-shell');
var options = {
    mode: 'text',
    pythonOptions: ['-u'],
    scriptPath: '/home/pi/node_modules/node-grovepi/sensors'
};


exports = module.exports = function(io) {
    //Evénènement déclenché à chaque nouvelle connection d'un client
    io.sockets.on("connection", function(socket) {


        PythonShell.run('gps.py', options, function(err, results) {
        if (err) {
        }
        else if (results) {
            socket.emit("GetLocation", results)
            }
    });


        socket.emit("GetDHTValue", currentDHTValue);
        socket.emit("GetBarometerValue", currentBarometerValue);
        socket.emit("GetLightValue", currentLightValue);
        //Evenement éxécuté lors d'un clique de l'utilisateur sur le bouton "Générer le graphe"
        socket.on("requestData", function(startDate, endDate, value) {
            dataHandler.RequestData(startDate, endDate, value);
            socket.emit("returnData", data);
        });

        socket.on("RequestPressureOneHourAgo", function(now){
           pressure = dataHandler.RequestPressureOneHourAgo(now)
           socket.emit("ReturnPressureOneHourAgo", pressure)
            
        });

        socket.on("SendTwittWithForecast", function(prev, address, temp, hum, press){
            console.log("send twitt")
        message = "A " + address + " : temp. : " + temp + "°C, hum. : " + hum + "%, press. : " + press + "hPa\nPrévisions : " + prev ;
           client.post('statuses/update', {status: message},  function(error, tweet, response){

            });
            
        });

                socket.on("SendTwittWithoutForecast", function(address, temp, hum, press){
            console.log("send twitt")
            message = "A " + address + " : temp. : " + temp + "°C, hum. : " + hum + "%, press. : " + press + "hPa";
           client.post('statuses/update', {status: message},  function(error, tweet, response){

            });
            
        });

        GrovePi.dhtEvent.on("dhtSensorChange", function(res) {
            socket.emit("GetDHTValue", currentDHTValue);
        });

        GrovePi.barometerEvent.on("barometerSensorChange", function(res) {
            socket.emit("GetBarometerValue", currentBarometerValue);
        });

        GrovePi.lightEvent.on("lightSensorChange", function(res) {
            socket.emit("GetLightValue", currentLightValue);
        });
    });
}
setInterval(function() {
    if (currentDHTValue && currentBarometerValue) {
        dataHandler.SaveData(currentDHTValue, currentBarometerValue, currentLightValue);
    }
}, 60000);

GrovePi.dhtEvent.on("dhtSensorChange", function(res) {
    currentDHTValue = res;
});

GrovePi.barometerEvent.on("barometerSensorChange", function(res) {

    currentBarometerValue = res;
});

GrovePi.lightEvent.on("lightSensorChange", function(res) {
    if(res != null && res != -1){
    currentLightValue = res;
    }

    else{
        currentLightValue = 0;
    }
});