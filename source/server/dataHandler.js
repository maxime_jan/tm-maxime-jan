//Import de la base de donnÃ©es LokiJS
var LokiDB = require('lokijs');
//CrÃ©ation de l'objet de la base de donnÃ©es en prÃ©cisant son emplacement
var database = new LokiDB("LokiDatabase.json");
var measurements;

database.loadDatabase({}, function() {
    measurements = database.getCollection("measurements");
    if (measurements === null) {
        measurements = database.addCollection("measurements");
    }
});

function SaveData(currentDHTValue, currentBarometerValue, currentLightValue) {
    var currentDate = new Date();
    console.log("Enregistrement d'une donnée");
    if (measurements != null) {
        measurements.insert({
            temperature: currentDHTValue[0],
            humidity: currentDHTValue[1],
            pressure: currentBarometerValue,
            light: currentLightValue,
            date: currentDate.getTime()
        });
    }
    database.saveDatabase();
}

function RequestData(startDate, endDate, values) {
    data = [];
    day = 86400000;
    hour = 3600000;
    endDate += day;
    endDate -= hour;
    startDate -= hour;

    var requestedData = measurements.find({
        '$and': [{
            date: {
                '$gt': startDate
            }
        }, {
            date: {
                '$lt': endDate
            }
        }]
    });

    if (include(values, "Température") && include(values, "Humidité")) {
        for (var i = 0; i < requestedData.length; i++) {

            data.push([requestedData[i].date, requestedData[i].temperature, requestedData[i].humidity]);
        }
    } else if (include(values, "Température") && include(values, "Pression")) {
        for (var i = 0; i < requestedData.length; i++) {

            data.push([requestedData[i].date, requestedData[i].temperature, requestedData[i].pressure]);
        }
    } else if (include(values, "Humidité") && include(values, "Pression")) {
        for (var i = 0; i < requestedData.length; i++) {

            data.push([requestedData[i].date, requestedData[i].humidity, requestedData[i].pressure]);
        }
    }

    else if (include(values, "Température") && include(values, "Luminosité")) {
        for (var i = 0; i < requestedData.length; i++) {

            data.push([requestedData[i].date, requestedData[i].temperature, requestedData[i].light]);
        }
    }

        else if (include(values, "Humidité") && include(values, "Luminosité")) {
        for (var i = 0; i < requestedData.length; i++) {

            data.push([requestedData[i].date, requestedData[i].humidity, requestedData[i].light]);
        }
    }


        else if (include(values, "Pression") && include(values, "Luminosité")) {
        for (var i = 0; i < requestedData.length; i++) {

            data.push([requestedData[i].date, requestedData[i].pressure, requestedData[i].light]);
        }
    }


     else if (include(values, "Température")) {
        for (var i = 0; i < requestedData.length; i++) {
            data.push([requestedData[i].date, requestedData[i].temperature]);
        }
    } else if (include(values, "Humidité")) {
        for (var i = 0; i < requestedData.length; i++) {

            data.push([requestedData[i].date, requestedData[i].humidity]);
        }
    } 

    else if (include(values, "Luminosité")) {
        for (var i = 0; i < requestedData.length; i++) {

            data.push([requestedData[i].date, requestedData[i].light]);
        }
    }

    else {
        for (var i = 0; i < requestedData.length; i++) {

            data.push([requestedData[i].date, requestedData[i].pressure]);
        }
    }
    return data;
}

function RequestPressureOneHourAgo(now){

press = 0
hour = 3600000;
semiMinute = 29999
startDate = now-hour-semiMinute-hour;
endDate = now-hour+semiMinute-hour;

var requestedData = measurements.find({
        '$and': [{
            date: {
                '$gt': startDate
            }
        }, {
            date: {
                '$lt': endDate
            }
        }]
    });
    if(requestedData[0]){
    press = requestedData[0].pressure
    }


return press


}


function include(list, item) {
    return (list.indexOf(item) != -1);
}

exports.SaveData = SaveData;
exports.RequestData = RequestData;
exports.RequestPressureOneHourAgo = RequestPressureOneHourAgo;