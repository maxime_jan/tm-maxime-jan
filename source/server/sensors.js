//Import du module node-grovepi
var GrovePi = require('node-grovepi').GrovePi;
//Evenements
var events = require('events');
var dhtEvent = new events.EventEmitter();
var barometerEvent = new events.EventEmitter();
var lightEvent = new events.EventEmitter();
//Chargement des commandes du GrovePi
var Commands = GrovePi.commands;
//Chargement de la carte "GrovePi"
var Board = GrovePi.board;
var board;
var dhtSensor;
var barometerSensor;
var lightSensor;
//Capteur de température et d'humidité relative
var DHTDigitalSensor = GrovePi.sensors.DHTDigital;
//Baromètre
var BarometerI2CSensor = GrovePi.sensors.BarometerI2C;

var LightSensor = GrovePi.sensors.LightAnalog;

var start = function() {
        console.log('starting');
        //CrÃ©ation de l'objet "board" qui correspond au GrovePi+
        board = new Board({
            debug: true,
            onError: function(err) {
                console.log('TEST ERROR');
                console.log(err);
            },

            onInit: function(res) {
                if (res) {
                    //Création de l'objet du capteur de température et humidité
                    dhtSensor = new DHTDigitalSensor(4, DHTDigitalSensor.VERSION.DHT22, DHTDigitalSensor.CELSIUS);
                    //Création de l'objet de l'objet du baromètre
                    barometerSensor = new BarometerI2CSensor(BarometerI2CSensor);

                    lightSensor = new LightSensor(1);
                    // Temps en millisecondes
                    dhtSensor.watch(100);
                    barometerSensor.watch(100);
                    lightSensor.watch(1000);

                    dhtSensor.on("change", function(res) {
                        dhtEvent.emit("dhtSensorChange", res);
                    })

                    barometerSensor.on("change", function(res) {
                        barometerEvent.emit("barometerSensorChange", res);
                    })

                    lightSensor.on("change", function(res){

                    if(res != null){
                        lightEvent.emit("lightSensorChange", res);
                    }
                    });

                } else {
                    console.log('TEST CANNOT START');
                }
            }
        })
        //Initialisation du GrovePi
        board.init()
    }
    //Fonction permettant d'arrêter le GrovePi

function onExit(err) {
    console.log('ending');
    board.close();
    process.removeAllListeners();
    process.exit();
    if (typeof err != 'undefined')
        console.log(err);
}
//Permet de détecter les énements "ctrl +c" et ainsi de lancer la fonction "OnExit" lors de l'arrêt du programme
process.on('SIGINT', onExit);

exports.startGP = start;
exports.dhtEvent = dhtEvent;
exports.barometerEvent = barometerEvent;
exports.lightEvent = lightEvent;